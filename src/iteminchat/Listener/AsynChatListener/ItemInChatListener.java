package iteminchat.Listener.AsynChatListener;

import com.mysql.jdbc.Util;
import iteminchat.Utils;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ItemInChatListener implements Listener
{
    private static final String TEXT_CHAT_ITEM_OPERATOR = "[item]";
    private static final String TEXT_CHAT_ITEM_OPERATOR_SHORTCUT = "[i]";

    @EventHandler(priority = EventPriority.LOWEST)
    public Boolean onItemPostInChat(AsyncPlayerChatEvent event)
    {
        Player player = event.getPlayer();
        PlayerInventory inventory = player.getInventory();
        ItemStack item = inventory.getItem(inventory.getHeldItemSlot());

        if(null == item){
            return false;
        }

        ItemMeta itemMeta = item.getItemMeta();

        if(null == itemMeta){
            return false;
        }

        String itemname = itemMeta.getDisplayName();

        if(itemname.equals("")){
            itemname = Utils.decapitalizeSeperatedString(item.getType().toString(), "_");
        }

        if(Material.AIR.equals(item.getType()))
        {
            return false;
        }

        if(!event.getMessage().contains("[item]")){
            return false;
        }
        String[] messageArr = event.getMessage().split("item");
        int messageOperatorAmount = messageArr.length;

        if(messageOperatorAmount <= 0){
            return false;
        }

        messageArr[0] = messageArr[0].substring(0, messageArr[0].length()-1);
        messageArr[1] = messageArr[1].substring(1);

        Map<Enchantment, Integer> enchantmentMap = item.getEnchantments();
        StringBuilder enchantments = new StringBuilder();

        Iterator enchantmentIterator = enchantmentMap.entrySet().iterator();

        while (enchantmentIterator.hasNext()){
            Map.Entry map = (Map.Entry)enchantmentIterator.next();
            Enchantment mapEnchantment = (Enchantment) map.getKey();
            Integer mapValue = (Integer) map.getValue();
            String enchantmentName = Utils.decapitalizeSeperatedString(mapEnchantment.getName(), "_");
            enchantments.append(enchantmentName).append(" ").append(mapValue.toString()).append("\n");
        }

        TextComponent subComponent = new TextComponent(ChatColor.AQUA + itemname+ ChatColor.WHITE);
        subComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
            ChatColor.AQUA + itemname + "\n" +
            ChatColor.WHITE + item.getType() + "\n" +
            ChatColor.AQUA + "x" + item.getAmount() + "\n" +
            ChatColor.WHITE + enchantments + "\n" +
            "").create()));
        TextComponent mainComponent = new TextComponent("");
        mainComponent.setColor(net.md_5.bungee.api.ChatColor.WHITE);
        mainComponent.addExtra(messageArr[0] + "");
        mainComponent.addExtra(subComponent);
        mainComponent.addExtra( "" + messageArr[1]);


        event.setCancelled(true);
        event.getPlayer().spigot().sendMessage(mainComponent);

        return true;
    }

}
