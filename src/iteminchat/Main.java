package iteminchat;

import org.bukkit.plugin.java.JavaPlugin;
import iteminchat.Listener.AsynChatListener.ItemInChatListener;

public class Main extends JavaPlugin
{
    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(new ItemInChatListener(), this);
    }

    @Override
    public void onDisable(){

    }
}
