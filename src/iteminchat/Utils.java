package iteminchat;

import java.beans.Introspector;
import java.util.ArrayList;
import java.util.List;

public class Utils
{
    public static String decapitalizeSeperatedString(String capitalized, String seperator)
    {
        StringBuilder decapitalizedString = new StringBuilder();

        if(!capitalized.contains(seperator)){
            return decapitalizeString(capitalized);
        }

        for(String string:capitalized.split(seperator)){
            decapitalizedString.append(decapitalizeString(string)).append(" ");
        }

        return decapitalizedString.toString();
    }

    public static String decapitalizeString(String capitalized)
    {
        String capital = capitalized.substring(0, 1).toUpperCase();
        String capitalizedWithoutCapital = capitalized.substring(1).toLowerCase();

        return capital + capitalizedWithoutCapital;
    }
}
